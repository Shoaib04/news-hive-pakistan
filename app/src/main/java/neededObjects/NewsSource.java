package neededObjects;

import android.os.Parcel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Shoaib on 5/28/2016.
 */
public class NewsSource implements Serializable{

    private static final long serialVersionUID = -6470090944414208496L;
    public String sourcename;
    public String rssRequestlink;
    public String type;
    public String last_Updated;
    public ArrayList<NewsItem> newsItemArrayList;
    public int NewItemsGotten = 0;

    public boolean toShow = true;
    public boolean loadingState = false;


    public int sourceID=0;
    private int idGenerator=0;

    public NewsSource()
    {
        newsItemArrayList = new ArrayList<>();
        toShow=true;
        last_Updated=null;
        sourceID = idGenerator;
        idGenerator++;

    }

    public NewsSource(String sourcename, String rssRequestlink, String type, boolean toShow) {
        this.sourcename = sourcename;
        this.rssRequestlink = rssRequestlink;
        this.type = type;
        this.toShow = toShow;
        this.newsItemArrayList = new ArrayList<NewsItem>();
        sourceID = idGenerator;
        idGenerator++;
    }

    public ArrayList<NewsItem> getNewsItemArrayList() {
        return newsItemArrayList;
    }

    public void setNewsItemArrayList(ArrayList<NewsItem> newsItemArrayList) {
        this.newsItemArrayList = newsItemArrayList;
    }

    public ArrayList<NewsItem> getFavorites()
    {
        ArrayList<NewsItem> tempList = new ArrayList<>();
        for (int i = 0; i < newsItemArrayList.size() ; i++)
        {
            if (newsItemArrayList.get(i).isFav==1)
            {
                tempList.add(newsItemArrayList.get(i));
            }
        }

        return tempList;
    }

    public ArrayList<NewsItem> getUnreads()
    {
        ArrayList<NewsItem> tempList = new ArrayList<>();
        for (int i = 0; i < newsItemArrayList.size() ; i++)
        {
            if (newsItemArrayList.get(i).isRead==0)
            {
                tempList.add(newsItemArrayList.get(i));
            }
        }

        return tempList;
    }

    public int getNewsItemIndex (String title)
    {
        for (int i = 0; i < newsItemArrayList.size() ; i++) {
            if (title.equals(newsItemArrayList.get(i).title))
            {
                return i;
            }
        }
        return -1;
    }
    public int getUnreadNews_Number()
    {
        int sumTotalOfUnread=0;
        for (int i = 0; i < newsItemArrayList.size() ; i++) {

            if (newsItemArrayList.get(i).isRead==0)
            {
                sumTotalOfUnread++;
            }
        }
        return sumTotalOfUnread;
    }
    public int getNewItemsGotten() {
        return NewItemsGotten;
    }

    public void setNewItemsGotten(int newItemsGotten) {
        NewItemsGotten = newItemsGotten;
    }

    protected NewsSource(Parcel in) {
        sourcename = in.readString();
        rssRequestlink = in.readString();
        type = in.readString();
        toShow = in.readByte() != 0;

    }

//    public static final Creator<NewsSource> CREATOR = new Creator<NewsSource>() {
//        @Override
//        public NewsSource createFromParcel(Parcel in) {
//            return new NewsSource(in);
//        }
//
//        @Override
//        public NewsSource[] newArray(int size) {
//            return new NewsSource[size];
//        }
//    };
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(sourcename);
//        dest.writeString(rssRequestlink);
//        dest.writeString(type);
//        dest.writeByte((byte) (toShow ? 1 : 0));
//    }
}
