package neededObjects;

/**
 * Created by Shoaib on 6/14/2016.
 */

public class DigitsUpdateEvent {

    public String Message = null;
    public String name = null;

    public DigitsUpdateEvent(String theName, String theMessage)
    {
        this.name = theName;
        this.Message = theMessage;
    }
}
