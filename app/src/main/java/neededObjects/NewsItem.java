package neededObjects;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Created by Shoaib on 5/30/2016.
 */
public class NewsItem implements Serializable, Parcelable, Comparable{

    public String title=null;
    public String link=null;
    public String description=null;
    public String descriptionWithHtml=null;
    public String sourceName=null;
    public String imageLink=null;
    public Byte Shown=0;
    public String contentEncoded=null;
    public String pubTime=null;
    public String author=null;
    public Byte isRead=0;
    public Byte isFav=0;


    public NewsItem ()
    {


    }

    public boolean ifEquals(NewsItem givenItem)
    {
        if (this.title==null || givenItem.title==null || this.description==null || givenItem.description==null) {

            if (this.title==null && givenItem.title==null)
            {
                if ( (this.description== null && givenItem.description==null) || (this.description.equals(givenItem.description)))
                {
                    if ( (this.pubTime==null && givenItem.pubTime==null) || (this.pubTime.equals(givenItem.pubTime)))
                    {
                        return true;
                    }
                }
            }
            else if (this.description==null && givenItem.description==null)
            {
                if ( (this.pubTime==null && givenItem.pubTime==null) || (this.pubTime.equals(givenItem.pubTime)))
                {
                    return true;
                }
            }
            return false;
        }
        else
        {
            if (this.title.equals(givenItem.title) && this.description.equals(givenItem.description))
            {
                if (this.pubTime!=null && givenItem.pubTime!=null && this.pubTime.equals(givenItem.pubTime))
                {
                    return true;
                }
                else if (this.pubTime==null && givenItem.pubTime==null)
                {
                    return true;
                }

                return false;
            }

        }

        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(link);
        dest.writeString(description);
        dest.writeString(descriptionWithHtml);
        dest.writeString(sourceName);
        dest.writeString(imageLink);
        dest.writeByte(Shown);
        dest.writeString(contentEncoded);
        dest.writeString(pubTime);
        dest.writeByte(isRead);
        dest.writeByte(isFav);

    }

    public static final Creator<NewsItem> CREATOR = new Creator<NewsItem>() {
        @Override
        public NewsItem createFromParcel(Parcel in) {
            return new NewsItem(in);
        }

        @Override
        public NewsItem[] newArray(int size) {
            return new NewsItem[size];
        }
    };

    private NewsItem (Parcel parcel)
    {
        title = parcel.readString();
        link = parcel.readString();
        description = parcel.readString();
        descriptionWithHtml = parcel.readString();
        sourceName = parcel.readString();
        imageLink = parcel.readString();
        Shown = parcel.readByte();
        contentEncoded = parcel.readString();
        pubTime = parcel.readString();
        isRead = parcel.readByte();
        isFav = parcel.readByte();
    }

    private long getDateTime (String givenPubTime)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            return dateFormat.parse(givenPubTime).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public int compareTo(Object another) {
        NewsItem givenItem = (NewsItem) another;


        Log.e("->"+givenItem.sourceName, givenItem.title + "--" + givenItem.pubTime);

        Log.e("->"+this.sourceName, this.title +"--"+ this.pubTime);

        long givenObjectsDate = getDateTime(givenItem.pubTime);
        long thisObjectsDate = getDateTime(this.pubTime);

        //Condition for Recent to Old News, That is Descending.
        long theTimeDiff = givenObjectsDate - thisObjectsDate;
        if (theTimeDiff >= 1)
        {
            return 1;
        }
        else if (theTimeDiff ==0)
        {
            return 0;
        }
        else if (theTimeDiff <= -1)
        {
            return -1;
        }
        return 0;
    }
}
