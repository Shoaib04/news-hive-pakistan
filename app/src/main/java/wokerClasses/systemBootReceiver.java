package wokerClasses;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import com.enigmaproapps.newshivepakistan.appConstants;

import java.util.Calendar;

/**
 * Created by Shoaib on 6/9/2016.
 */

public class systemBootReceiver extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d("Boot Notify","System Boot BroadCast caught");
        registerToAlarmManager(context);
    }

    private void registerToAlarmManager(Context context) {
        Calendar calendar = Calendar.getInstance();

        Intent intent = new Intent(context, alarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(),
                appConstants.AlarmManger_INTERVAL_HALFHOUR, pendingIntent);


    }
}
