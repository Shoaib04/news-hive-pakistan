package wokerClasses;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.enigmaproapps.newshivepakistan.AppPrefsManager;
import com.enigmaproapps.newshivepakistan.MainActivity;
import com.enigmaproapps.newshivepakistan.R;
import com.enigmaproapps.newshivepakistan.appConstants;
import com.enigmaproapps.newshivepakistan.globalClass;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import neededObjects.NewsItem;
import neededObjects.NewsSource;

/**
 * Created by Shoaib on 6/9/2016.
 */

public class backgroundServiceForAlarm extends Service {

    private static final int NOTIFICATION_ID_FOREGROUND = 1344;

    private boolean isRunning;
    private Context context;
    private Thread backgroundThread;
    private String MAP_STORAGE_FILENAME = appConstants.File_allSourcesFeedHashMap;
    private ArrayList<NewsSource> newsSourceArrayList;
    private int total_New_News_Items = 0;
    private SimpleDateFormat dateFormat;

    @Override
    public void onCreate() {
        super.onCreate();

        this.isRunning = false;
        this.context = this;
        dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        this.backgroundThread = new Thread(myFeedTask);
        total_New_News_Items = 0;
        //((globalClass) getApplication()).MakeToast();
        // android.os.Debug.waitForDebugger();

    }

    private Runnable myFeedTask = new Runnable() {
        @Override
        public void run() {
            // Will be doing work Here 
            Log.d("serviceTask", "run method of service");

            //  builNotification(100);
            if (AppPrefsManager.getInstance().hasContextAlready()) {

                Log.e("backGrou_Service", "Background service says context exists");

                if (AppPrefsManager.getInstance().isBackgroundSyncEnabled()) {
                    //Load the new feed and store it in the memory
                    loadFeedsAndStoreInMap();
                    //Now build a notification that shows how many new articles have been fetched.
                    builNotification();
                }

            } else {
                Log.e("backGrou_Service", "Background service says NO context exists");
                //((globalClass) getApplication()).MakeToast();
            }
            // When work is done stop this instance of Service
            stopForeground(true);
            stopSelf();
        }
    };

    private void cleanOldData(ArrayList<NewsSource> mList) {
        long intervalVar = AppPrefsManager.getInstance().getWhenRemoveReadItems();
        if (intervalVar == 0) {
            return;
        }
        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();

        for (int i = 0; i < mList.size(); i++) {
            ArrayList<NewsItem> theNewsList = mList.get(i).getNewsItemArrayList();
            cleanGivenListFromOldData(theNewsList, currentDate, intervalVar);

        }

    }

    private void cleanGivenListFromOldData(ArrayList<NewsItem> theNewsList, Date currentDate, long timeInterval) {

        for (int i = 0; i < theNewsList.size(); i++) {

            NewsItem mTempItem = theNewsList.get(i);
            if (mTempItem.pubTime == null) {
                continue;
            }
            try {
                Date mNewsDate = dateFormat.parse(mTempItem.pubTime);
                long timeDifference = currentDate.getTime() - mNewsDate.getTime();
                long difference_Hours = getDateDifference(mNewsDate,currentDate);
                if (timeDifference >= timeInterval) {
                    theNewsList.remove(i);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    private void adjustThisNewsSourceInTheMainList(NewsSource thesource) {

        Log.d("backService--4","Start Removing Duplicates Code");
        ArrayList<NewsItem> currentListToCheckForDuplicates = thesource.getNewsItemArrayList();
        //android.os.Debug.waitForDebugger();
        for (int i = 0; i < currentListToCheckForDuplicates.size() ; i++) {

            for (int j = i+1; j < currentListToCheckForDuplicates.size() ; j++) {
                NewsItem currentNewsItem = currentListToCheckForDuplicates.get(j);
                Log.d("backService--5","Removing Duplicates for: -" + thesource.sourcename + "-" + currentListToCheckForDuplicates.get(i).title+" index: " + String.valueOf(i));
                Log.d("backService--6","Removing the Duplicate: -" + thesource.sourcename + "-" + currentNewsItem.title+" index: " + String.valueOf(j));
                if (currentNewsItem==null || currentListToCheckForDuplicates.get(i)==null)
                {
                    continue;
                }
                if (currentNewsItem.ifEquals(currentListToCheckForDuplicates.get(i)))
                {

                    currentListToCheckForDuplicates.remove(j);
                }
            }
        }

        int index = -1;
        for (int i = 0; i < newsSourceArrayList.size(); i++) {
            if (thesource.sourcename.equals(newsSourceArrayList.get(i).sourcename)) {
                index = i;
                newsSourceArrayList.remove(index);
                newsSourceArrayList.add(index, thesource);
                break;
            }
        }
    }

    private void loadFeedsAndStoreInMap() {
        fileStorageHandler storageHandler = new fileStorageHandler(context);
        //waitForDebugger();
        newsSourceArrayList = ((globalClass) getApplication()).getNewsSourcesList();
        if (newsSourceArrayList == null) {
            newsSourceArrayList = (ArrayList<NewsSource>) storageHandler.readObject(ArrayList.class, appConstants.File_allSourcesFeedHashMap);
            if (newsSourceArrayList == null) {
                //Shows there was no previously stored feeds, so we have to load them
                ((globalClass) getApplication()).fillSources();
                newsSourceArrayList = ((globalClass) getApplication()).getNewsSourcesList();
            } else if (newsSourceArrayList.size() <= 1) {
                //Shows there was no previously stored feeds, so we have to load them
                ((globalClass) getApplication()).fillSources();
                newsSourceArrayList = ((globalClass) getApplication()).getNewsSourcesList();
            }
        }

        cleanOldData(newsSourceArrayList);

        for (int i = 0; i < newsSourceArrayList.size(); i++) {
            if (newsSourceArrayList.get(i).toShow) {
                NewsSource tempNewsSource = retriveOnlineFeedForMe(newsSourceArrayList.get(i));
                if (tempNewsSource != null) {
                    adjustThisNewsSourceInTheMainList(tempNewsSource);
                }

            }
        }

        if (newsSourceArrayList != null) {

            storageHandler.writeObject(MAP_STORAGE_FILENAME, newsSourceArrayList);
            Log.d("bgService Storing", "Bg Service stored data");
        }

        return;

    }

    private long getDateDifference(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        elapsedHours = elapsedDays * 24 + elapsedHours;
        return elapsedHours;
    }

    public NewsSource retriveOnlineFeedForMe(NewsSource source) {
        if (source.toShow) {

            InputStream feedURL = getInputStream(source.rssRequestlink);
            NewsSource tempNewsSource;

            feedParserClass feedParser = new feedParserClass(source);

            if (feedURL == null) {
                Log.e("feedURL", "Feed URL could not be formed");
            } else {
                try {

                    tempNewsSource = feedParser.ParseGivenFeed(feedURL);
                    total_New_News_Items = total_New_News_Items + tempNewsSource.NewItemsGotten;

                    return tempNewsSource;
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }


        }

        return null;

    }

    public InputStream getInputStream(String link) {
        try {
            URL url = new URL(link);
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }

    private void builNotification() {
        //Return when if right below returns false; that means Notification is Disabled.
        if (!AppPrefsManager.getInstance().getIfNotsForAutoSycnEnabled()) {
            return;
        }
        String numOfArticlesFetchedText;
        if (total_New_News_Items == 0) {
            numOfArticlesFetchedText = "Feed is already UptoDate";
        } else {
            numOfArticlesFetchedText = String.valueOf(total_New_News_Items) + " Articles";
        }
        int iconID;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
        {
            iconID = R.drawable.newshinve_notif_icon48;
        }
        else
        {
            iconID = R.mipmap.newshivelogo;
        }
        NotificationCompat.Builder notBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(iconID)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setContentText("News articles fetched")
                .setContentInfo(numOfArticlesFetchedText)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .setColor(Color.argb(255,2,168,90))
                .setDefaults(Notification.DEFAULT_SOUND);
        //If Vibration for notification is enabled in Pres than set Vibration Pattern.
        if (AppPrefsManager.getInstance().getIfVibrationForAutoSycnEnabled()) {
            notBuilder.setVibrate(new long[]{1000, 1000});
        }
        Intent resultIntent = new Intent(context, MainActivity.class);
        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        notBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(8544, notBuilder.build());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //if the App is currently Running that is app is Open than this service does not Proceed.
        if (((globalClass) getApplication()).isAppRunning_HasMainActivityChangedMe) {

            stopSelf();
            return START_NOT_STICKY;
        }

        //if the Thread is already Running do not run it again.
        if (!isRunning) {
            this.isRunning = true;
            this.backgroundThread.start();
        }
        else
        {
            //This code executes when service is already running
            return  START_NOT_STICKY;
        }

        int iconID;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
        {
            iconID = R.drawable.newshinve_notif_icon48;
        }
        else
        {
            iconID = R.mipmap.newshivelogo;
        }
        //This is the Foreground Notification that tells the user that this app is running
        //and is fetching new articles from all news sources.
        NotificationCompat.Builder notificationCompat = new NotificationCompat.Builder(this)
                .setSmallIcon(iconID)
                .setContentTitle("Updating news feed")
                .setTicker("News feed update started")
                .setContentText("Fetching new articles")
                .setProgress(0, 0, true)
                .setColor(Color.argb(255,2,168,90));
        Notification mnotific = notificationCompat.build();
        startForeground(NOTIFICATION_ID_FOREGROUND, mnotific);

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.isRunning = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
