package wokerClasses;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.enigmaproapps.newshivepakistan.appConstants;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import neededObjects.NewsItem;
import neededObjects.NewsSource;

/**
 * Created by Shoaib on 5/31/2016.
 */
public class RssFeedLoaderService extends IntentService {

    private static final String Name_WokerThread = "WrokerThread";
    private Context context;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */

    public RssFeedLoaderService() {
        super(Name_WokerThread);

    }

    public RssFeedLoaderService(Context mcon) {
        super(Name_WokerThread);
        context = mcon;
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("rssService", "Service Started");

        NewsSource receivedObject = (NewsSource) intent.getSerializableExtra(appConstants.IntentParam_globalToRssService_NewsSourceObject);
        InputStream feedURL = getInputStream(receivedObject.rssRequestlink);

        ArrayList<NewsItem> rssItems = null;
        feedParserClass feedParser = new feedParserClass(receivedObject);

        if (feedURL == null) {
            Log.e("feedURL", "Feed URL could not be formed");
            Bundle b = new Bundle();
            b.putSerializable(appConstants.IntentParam_serviceSendingResultingNewsSourceObject, receivedObject);
            android.os.ResultReceiver receiver = intent.getParcelableExtra(appConstants.Service_globalClass_ResultReceiverName);
            receiver.send(0, b);
        } else {
            try {
               // rssItems
                Log.d("intentService",receivedObject.sourcename+" intentservice Loading");
                NewsSource mItem= feedParser.ParseGivenFeed(feedURL);
                Bundle b = new Bundle();
                b.putSerializable(appConstants.IntentParam_serviceSendingResultingNewsSourceObject, mItem);
                android.os.ResultReceiver receiver = intent.getParcelableExtra(appConstants.Service_globalClass_ResultReceiverName);
                receiver.send(0, b);

            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }

    public InputStream getInputStream(String link) {
        try {
            URL url = new URL(link);
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }
}
