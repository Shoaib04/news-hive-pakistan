package com.enigmaproapps.newshivepakistan;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import neededObjects.NewsSource;

import static com.enigmaproapps.newshivepakistan.appConstants.NAME_APPTHEME_NAIJA;

public class sourceSelectionAcitivity extends AppCompatActivity {

    private ArrayList<NewsSource> mList;
    private ListView mListView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCorrectTheme();
        setContentView(R.layout.source_selection_acitivity);

        toolbar = (Toolbar) findViewById(R.id.appBar_SourceActivity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mList = ((globalClass)getApplication()).getNewsSourcesList();
        mListView = (ListView) findViewById(R.id.selectionActivityListView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox b= (CheckBox)view.findViewById(R.id.listItemCheckBox);
                int pos=position;
                    if (!b.isChecked())
                    {
                        b.setChecked(true);
                        ((globalClass)getApplication()).getNewsSourcesList().get(position).toShow=true;
                        boolean check =((globalClass)getApplication()).getNewsSourcesList().get(position).toShow;
                    }
                    else
                    {
                        b.setChecked(false);
                        ((globalClass)getApplication()).getNewsSourcesList().get(position).toShow=false;
                        boolean check =((globalClass)getApplication()).getNewsSourcesList().get(position).toShow;
                        if (check)
                            Log.e("haha","heheh");
                        else{
                            Log.e("hoho","-----");
                        }
                    }

            }
        });
        mListView.setAdapter(new listAdapter());

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(sourceSelectionAcitivity.this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        super.onBackPressed();
    }

    private void setCorrectTheme() {
        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
        }
        String name = ((globalClass) getApplication()).getApp_CurrentThemeName();
        if (name.equals(NAME_APPTHEME_NAIJA)) {
            setTheme(R.style.AppThemeNaija_NoActionBar);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
            }
        }

        if (name.equals("AppThemeLight")) {
            setTheme(R.style.AppThemeLight);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark_Light));
            }
        }

        if (name.equals("AppThemeDark")) {
            setTheme(R.style.AppThemeDark);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            NavUtils.navigateUpFromSameTask(this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    private class listAdapter extends BaseAdapter{


        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public NewsSource getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
            {
                LayoutInflater layoutInflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.singlelistitem_selectionactivity,null);
            }
            TextView mTextView = ((TextView)convertView.findViewById(R.id.listItemTextView));
            mTextView.setText(mList.get(position).sourcename);
            CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.listItemCheckBox);
            checkBox.setClickable(false);

            //Set TExtAppearance
            if (mTextView!=null)
            {
                TypedValue mval = new TypedValue();
                sourceSelectionAcitivity.this.getTheme().resolveAttribute(android.R.attr.textAppearanceLarge, mval, true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mTextView.setTextAppearance(mval.resourceId);
                } else {
                    mTextView.setTextAppearance(sourceSelectionAcitivity.this, mval.resourceId);
                }
            }


            if (((globalClass)getApplication()).getNewsSourcesList().get(position).toShow==true)
            {
                checkBox.setChecked(true);
            }else
            {
                checkBox.setChecked(false);
            }
            return convertView;
        }
    }
}
