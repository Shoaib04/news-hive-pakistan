package com.enigmaproapps.newshivepakistan;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.greenrobot.event.EventBus;
import neededObjects.DigitsUpdateEvent;
import neededObjects.EventfeedLoaded;
import neededObjects.NewsItem;
import neededObjects.NewsSource;

import static com.enigmaproapps.newshivepakistan.appConstants.IntentParam_ListIndex_MaintoDetailActivity;
import static com.enigmaproapps.newshivepakistan.appConstants.NAME_APPTHEME_NAIJA;
import static com.enigmaproapps.newshivepakistan.appConstants.navMenu_Catergory_AllUnreads;
import static com.enigmaproapps.newshivepakistan.appConstants.navMenu_Catergory_Favourites;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView;
    RecyclerView mRecylerView_NewsFeedList;
    recylerAdapter mainActivity_RecyclerAdapter;
    ArrayList<NewsSource> NewsSoucrceList_Current;
    NewsSource CurrentNewsSource;
    ArrayList<NewsItem> NewsItemsList_Current;
    String currentlySelectedNewsSourceName = null;
    Menu navMenuRef;
    String whoEnablesLoader = "";
    ProgressBar progressBar;
    boolean MenuFilled = false;
    String sortManner = appConstants.MainActivity_ListSort_Mode_RecentToOld;
    private boolean ifFavOrUnreadSelected = false;
    //If this boolean is true, than a util method finds correct index in original list and than
    //passes this to next activity so that correct NewsItem is opened in NextActivity from original List
    private boolean ifSearchFilterActivated = false;
    private NewsSource newsSource_BeingRefreshed_ByFAB = null;

    //Handler and Runnable to set AllUnreads periodically if its size is zero.
    private android.os.Handler mHandler;
    private Runnable mRunnable_For_mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setCorrectTheme();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ((globalClass) getApplication()).isAppRunning_HasMainActivityChangedMe = true;
        progressBar = (ProgressBar) findViewById(R.id.progressBarMain);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setElevation(6.0f);
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_menu_refresh);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CurrentNewsSource==null)
                {
                    return;
                }
                if (CurrentNewsSource.sourcename.equals(appConstants.navMenu_Catergory_AllUnreads) || CurrentNewsSource.sourcename.equals(appConstants.navMenu_Catergory_Favourites))
                {
                    return;
                }
                if (CurrentNewsSource != null) {
                    Snackbar.make(view, "Updating " + CurrentNewsSource.sourcename + " NewsFeed", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    progressBar.setVisibility(View.VISIBLE);
                    newsSource_BeingRefreshed_ByFAB = CurrentNewsSource;
                    ((globalClass) getApplication()).RefreshASingleSource(CurrentNewsSource.sourcename);
                    mainActivity_RecyclerAdapter.notifyDataSetChanged();
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //change6
        registerForContextMenu(navigationView.getMenu().findItem(R.id.nav_displaySettings).getActionView());
        navigationView.getMenu().findItem(R.id.nav_displaySettings).getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContextMenu(v);
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progressBarMain);
        //Initialize the MainAcivity List that would contain all news for currently selected Source
        NewsSoucrceList_Current = new ArrayList<NewsSource>();

        navMenuRef = navigationView.getMenu();

        //**
        //Setting up ListView and setting it's adapter and all that....
        mRecylerView_NewsFeedList = (RecyclerView) findViewById(R.id.newsFeedRecylerView_MainActivity);
        mainActivity_RecyclerAdapter = new recylerAdapter();
        mRecylerView_NewsFeedList.setAdapter(mainActivity_RecyclerAdapter);
        setRecyclerView_ViewStyle(-110189);

        //Set Null shows that when activity is created No source is pressed to be refreshed
        //By pressing FAB.
        newsSource_BeingRefreshed_ByFAB = null;
        //Register for events from the event bus
        EventBus.getDefault().register(this);


        //This code is called only for the first Time
        if (((globalClass) getApplication()).toLoad) {
            ((globalClass) getApplication()).mainRegisteredEvent();
            MobileAds.initialize(getApplicationContext(),appConstants.AdMob_AppID);
            ((globalClass) getApplication()).toLoad = false;
            drawer.openDrawer(GravityCompat.START);
            set_AllUnreads_ByDefault_WhenActivity_Created();
            if (mHandler==null)
            {
                mHandler = new Handler();
                mRunnable_For_mHandler = new Runnable() {
                    @Override
                    public void run() {
                        if (getUnreads_NewsSource().getNewsItemArrayList().size() > 0)
                        {
                            set_AllUnreads_ByDefault_WhenActivity_Created();
                           // mHandler.removeCallbacks(mRunnable_For_mHandler);
                        }
                        else
                        {
                            mHandler.postDelayed(mRunnable_For_mHandler,2000);
                        }

                    }
                };
            }
            else if (getUnreads_NewsSource().getNewsItemArrayList().size()> 0)
            {
                //Do Nothing just let the code go
            }
            if (getUnreads_NewsSource().getNewsItemArrayList().size()==0)
            {
                mHandler.postDelayed(mRunnable_For_mHandler,2000);
            }

        }

        //This is called only when Activity is Recreated.....Not the First time when Application starts
        if (((globalClass) getApplication()).canMainCallFillMenu) {
            fillMenu(navigationView.getMenu());
            ((globalClass) getApplication()).giveDigitsAccordingTofeedsInStorageToNavMenu(navigationView.getMenu());
        }

        if (globalClass.MainActivityStatePreserveBundle_SourceName != null) {
            recoverPreviousStateForActivity();

        }

        AdView mAdView = (AdView) findViewById(R.id.mainActivityBannerAd);
        mAdView.setVisibility(View.VISIBLE);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }


    private NewsSource getMyFavourites_NewsSource() {

        NewsSource MyFavsNewsSource = globalClass.NewsSource_MyFavorites;
        if (MyFavsNewsSource.newsItemArrayList == null) {
            MyFavsNewsSource.newsItemArrayList = new ArrayList<>();
        }
        MyFavsNewsSource.newsItemArrayList.clear();
        for (int i = 0; i < ((globalClass) getApplication()).getNewsSourcesList().size(); i++) {
            MyFavsNewsSource.newsItemArrayList.addAll(((globalClass) getApplication()).getNewsSourcesList().get(i).getFavorites());
        }
        return MyFavsNewsSource;
    }

    private NewsSource getUnreads_NewsSource() {

        NewsSource AllUnreadsNewsSource = globalClass.NewsSource_AllUnreads;
//        if (AllUnreadsNewsSource.newsItemArrayList == null) {
//            AllUnreadsNewsSource.newsItemArrayList = new ArrayList<>();
//        }
//        AllUnreadsNewsSource.newsItemArrayList.clear();
//        for (int i = 0; i < ((globalClass) getApplication()).getNewsSourcesList().size(); i++) {
//            AllUnreadsNewsSource.newsItemArrayList.addAll(((globalClass) getApplication()).getNewsSourcesList().get(i).getUnreads());
//        }
        return AllUnreadsNewsSource;
    }

    private void recoverPreviousStateForActivity() {

        if (globalClass.MainActivityStatePreserveBundle_SourceName.equals(navMenu_Catergory_AllUnreads) || globalClass.MainActivityStatePreserveBundle_SourceName.equals(navMenu_Catergory_Favourites)) {

            currentlySelectedNewsSourceName = globalClass.MainActivityStatePreserveBundle_SourceName;
            if (globalClass.MainActivityStatePreserveBundle_SourceName.equals(navMenu_Catergory_AllUnreads)) {
                CurrentNewsSource = globalClass.NewsSource_AllUnreads;
                NewsItemsList_Current = CurrentNewsSource.getNewsItemArrayList();
                getSupportActionBar().setTitle(navMenu_Catergory_AllUnreads + "(" + CurrentNewsSource.getUnreads().size() + ")");
            } else {
                CurrentNewsSource = globalClass.NewsSource_MyFavorites;
                NewsItemsList_Current = CurrentNewsSource.getNewsItemArrayList();
                getSupportActionBar().setTitle(navMenu_Catergory_Favourites + "(" + CurrentNewsSource.getUnreads().size() + ")");
            }

            NewsSoucrceList_Current = ((globalClass) getApplication()).getNewsSourcesList();
            mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
            mainActivity_RecyclerAdapter.notifyDataSetChanged();

            // restore index and position
            ((LinearLayoutManager) mRecylerView_NewsFeedList.getLayoutManager()).scrollToPosition(globalClass.MainActivityStatePreserveBundle_ScrollPosition);


        } else {
            NewsSource ourSource = ((globalClass) getApplication()).getNewsSourceByName(globalClass.MainActivityStatePreserveBundle_SourceName);
            CurrentNewsSource = ourSource;

            NewsItemsList_Current = ((globalClass) getApplication()).getNewsArrayList_WhichIs_Maintained();
            NewsSoucrceList_Current = ((globalClass) getApplication()).getNewsSourcesList();
            mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
            mainActivity_RecyclerAdapter.notifyDataSetChanged();

            // restore index and position
            ((LinearLayoutManager) mRecylerView_NewsFeedList.getLayoutManager()).scrollToPosition(globalClass.MainActivityStatePreserveBundle_ScrollPosition);
            Menu ourMenu = navigationView.getMenu();
            for (int i = 0; i < ourMenu.size(); i++) {
                if (ourMenu.getItem(i).getTitle().equals(CurrentNewsSource.sourcename)) {
                    ourMenu.getItem(i).setChecked(true);

                    getSupportActionBar().setTitle(ourMenu.getItem(i).getTitle() + "(" + ((TextView) ourMenu.getItem(i).getActionView().findViewById(R.id.actionViewText)).getText() + ")");
                }
            }


        }

        globalClass.MainActivityStatePreserveBundle_SourceName = null;
        globalClass.MainActivityStatePreserveBundle_NewsItemsListToMaintain = null;
        globalClass.MainActivityStatePreserveBundle_ScrollPosition = 0;
        return;

    }

    private void setCorrectTheme() {
        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
        }
        String name = ((globalClass) getApplication()).getApp_CurrentThemeName();
        if (name.equals(NAME_APPTHEME_NAIJA)) {
            setTheme(R.style.AppThemeNaija_NoActionBar);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
            }
        }

        if (name.equals("AppThemeLight")) {
            setTheme(R.style.AppThemeLight);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark_Light));
            }
        }

        if (name.equals("AppThemeDark")) {
            setTheme(R.style.AppThemeDark);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
            }
        }
    }

    private void setRecyclerView_ViewStyle(int layoutID) {
        if (globalClass.currentList_Style == R.layout.news_list_item_ascard) {
            mRecylerView_NewsFeedList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            mRecylerView_NewsFeedList.setAdapter(mainActivity_RecyclerAdapter);
        } else if (globalClass.currentList_Style == R.layout.news_list_item_rectangular) {
            if (layoutID == R.id.subAction_GridLayout) {
                mRecylerView_NewsFeedList.setLayoutManager(new GridLayoutManager(MainActivity.this, 3));
                mRecylerView_NewsFeedList.setAdapter(mainActivity_RecyclerAdapter);
            } else {
                mRecylerView_NewsFeedList.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                mRecylerView_NewsFeedList.setAdapter(mainActivity_RecyclerAdapter);
            }

        } else if (globalClass.currentList_Style == R.layout.news_list_item_simple) {
            mRecylerView_NewsFeedList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            mRecylerView_NewsFeedList.setAdapter(mainActivity_RecyclerAdapter);
        } else if (globalClass.currentList_Style == R.layout.news_list_item_cardmagazine) {
            mRecylerView_NewsFeedList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            mRecylerView_NewsFeedList.setAdapter(mainActivity_RecyclerAdapter);
        }
    }

    public void onEvent(DigitsUpdateEvent event) {
        Log.d("DigitsUpdateEvent--", "DigitsUpdateEvent Received");
        String Event_SourceName = event.name;
        if (event.Message.equals("updateDigits")) {
            TextView mTextView;
            for (int i = 0; i < navMenuRef.size(); i++) {
                if (Event_SourceName.equals(navMenuRef.getItem(i).getTitle())) {
                    mTextView = (TextView) navMenuRef.getItem(i).getActionView().findViewById(R.id.actionViewText);
                    //int no = ((globalClass) getApplication()).getNewsSourceByName(Event_SourceName).getNewItemsGotten();
                    int no = ((globalClass) getApplication()).getNewsSourceByName(Event_SourceName).getUnreadNews_Number();
                    mTextView.setText(Integer.toString(no));
                    break;
                }
            }
        }
        if (CurrentNewsSource != null) {

            if (Event_SourceName.equals(CurrentNewsSource.sourcename)) {
                Log.d("TitleNumberUpdated", "DigitsUpdateEvent--> unreadNewsNumber for selected source set in title");
                CurrentNewsSource = ((globalClass) getApplication()).getNewsSourceByName(Event_SourceName);
                NewsItemsList_Current = CurrentNewsSource.getNewsItemArrayList();
                mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(CurrentNewsSource.getNewsItemArrayList());
                mainActivity_RecyclerAdapter.notifyDataSetChanged();
                getSupportActionBar().setTitle(CurrentNewsSource.sourcename + "(" +Integer.toString(CurrentNewsSource.getUnreadNews_Number())+ ")" );
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    // This method will be called when a HelloWorldEvent is posted
    public void onEvent(EventfeedLoaded event) {

        //Global Class sends event to fill the Menu
        if (event.Message != null && event.Message.equals("fillMenu")) {
            Menu mMenu = navigationView.getMenu();
            fillMenu(mMenu);
            //Once the menu has been populated -> give digits that are the number of unread news to nav items.
            ((globalClass) getApplication()).giveDigitsAccordingTofeedsInStorageToNavMenu(mMenu);
            ((globalClass) getApplication()).canMainCallFillMenu = true;
            return;
        }

        String Event_SourceName = event.getSourceName_feedFor();

        //Case when there is no Network Connection
        String noConnection = "noConnection";
        if (Event_SourceName.equals(noConnection)) {
            Toast.makeText(MainActivity.this, "No Active Network Access", Toast.LENGTH_LONG).show();
            return;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.newsFeedRecylerView_MainActivity) {

            AdapterView.AdapterContextMenuInfo info;
            try {
                // Casts the incoming data object into the type for AdapterView objects.
                info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            } catch (ClassCastException e) {
                // If the menu object can't be cast, logs an error.
                return;
            }
            NewsItem gottenItem = mainActivity_RecyclerAdapter.getItem_customMethod(info.position);

            // if your column name is "name"
            menu.setHeaderTitle(gottenItem.title);
            MenuInflater inflater = getMenuInflater();
            getMenuInflater().inflate(R.menu.main_listview_item_contextmenu, menu);
        } else {
            getMenuInflater().inflate(R.menu.contextualmenu_displaysettings, menu);
            String currentThemeName = ((globalClass) getApplication()).getApp_CurrentThemeName();

        }

    }

    private void fillMenu(Menu menu) {

        if (!MenuFilled) {
            MenuFilled = true;
            //Populate ArrayList and navdrawer menu with the NewsSources that have toShow=true ....
            NewsSoucrceList_Current.clear();
            for (int i = 0; i < ((globalClass) getApplication()).getNewsSourcesList().size(); i++) {
                if (((globalClass) getApplication()).getNewsSourcesList().get(i).toShow == true) {
                    Log.d("filling menu...", ((globalClass) getApplication()).getNewsSourcesList().get(i).sourcename);
                    NewsSoucrceList_Current.add(((globalClass) getApplication()).getNewsSourcesList().get(i));
                    menu.add(R.id.sourcesGroup, i, i, ((globalClass) getApplication()).getNewsSourcesList().get(i).sourcename);
                    menu.findItem(i).setActionView(R.layout.actionview_navmenu_newssources);
                    View v = menu.findItem(i).getActionView();
                    Log.e("", "");
                } else {

                }
            }
        }


    }

    private void deleteCurrentItem(int position) {
        NewsItemsList_Current.remove(position);
        mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
        mainActivity_RecyclerAdapter.notifyDataSetChanged();
    }

    private void markCurrentItemReadorUnread(int position) {
        if (NewsItemsList_Current.get(position).isRead == 0) {
            NewsItemsList_Current.get(position).isRead = 1;
        } else if (NewsItemsList_Current.get(position).isRead == 1) {
            NewsItemsList_Current.get(position).isRead = 0;
        }
        mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
        mainActivity_RecyclerAdapter.notifyDataSetChanged();
    }

    private void markReadUntilHere(int position) {
        if (position < NewsItemsList_Current.size()) {
            for (int i = 0; i < position; i++) {

                NewsItemsList_Current.get(i).isRead = 1;
            }
        }
        mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
        mainActivity_RecyclerAdapter.notifyDataSetChanged();

    }

    private void markReadToBottom(int position) {
        for (int i = 0; i < NewsItemsList_Current.size(); i++) {

            NewsItemsList_Current.get(i).isRead = 1;
        }
        mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
        mainActivity_RecyclerAdapter.notifyDataSetChanged();
    }

    private void addToFavorites(int position) {
        //Add Item at given position to Favorites
        NewsItemsList_Current.get(position).isFav = 1;
    }

    private void deleteAllRead() {
        if (NewsItemsList_Current != null && NewsItemsList_Current.size() != 0) {
            int size = NewsItemsList_Current.size();
            for (int i = 0; i < size; i++) {
                if (NewsItemsList_Current.get(i).isRead == 1) {
                    NewsItemsList_Current.remove(i);
                    size = NewsItemsList_Current.size();
                    i = -1;
                }
            }
            mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
            mainActivity_RecyclerAdapter.notifyDataSetChanged();
        } else {
            return;
        }

    }

    private void markAllRead() {
        if (NewsItemsList_Current != null && NewsItemsList_Current.size() != 0) {
            markReadToBottom(0);
        } else {
            return;
        }

    }

    private void showAllReadOnly() {
        if (CurrentNewsSource != null) {
            if (NewsItemsList_Current != null && NewsItemsList_Current.size() != 0) {
                ArrayList<NewsItem> listForOnlyReadShow = new ArrayList<>();
                for (int i = 0; i < NewsItemsList_Current.size(); i++) {
                    if (NewsItemsList_Current.get(i).isRead == 1) {
                        listForOnlyReadShow.add(NewsItemsList_Current.get(i));
                    }

                }

                mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(listForOnlyReadShow);
                mainActivity_RecyclerAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        //AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.actionListNews_Delete:
                deleteCurrentItem(mainActivity_RecyclerAdapter.getPost_ContextMenu_CurentFor());
                return true;
            case R.id.actionListNews_ToggleRead:
                markCurrentItemReadorUnread(mainActivity_RecyclerAdapter.getPost_ContextMenu_CurentFor());
                return true;
            case R.id.actionListNews_MarkReadUntilHere:
                markReadUntilHere(mainActivity_RecyclerAdapter.getPost_ContextMenu_CurentFor());
                return true;
            case R.id.actionListNews_MarkReadToBottom:
                markReadToBottom(mainActivity_RecyclerAdapter.getPost_ContextMenu_CurentFor());
                return true;
            case R.id.actionListNews_AddToFavorites:
                addToFavorites(mainActivity_RecyclerAdapter.getPost_ContextMenu_CurentFor());
                return true;
        }
        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);


        if (item.getItemId() == R.id.darkTheme) {

            ((globalClass) getApplication()).setApp_CurrentThemeName("AppThemeDark");
            item.setChecked(true);

            //Save the State Before Restarting
            saveMainActivityState();

            Intent intent = getIntent();
            finish();
            startActivity(intent);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
            }
        } else if (item.getItemId() == R.id.lightTheme) {
            // setTheme(R.style.AppThemeLight);
            ((globalClass) getApplication()).setApp_CurrentThemeName("AppThemeLight");
            item.setChecked(true);

            //Save the State Before Restarting
            saveMainActivityState();

            Intent intent = getIntent();
            finish();
            startActivity(intent);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Light));
            }
            Toast.makeText(this, "Light", Toast.LENGTH_SHORT).show();
        } else if (item.getItemId() == R.id.defaultTheme) {

            ((globalClass) getApplication()).setApp_CurrentThemeName(NAME_APPTHEME_NAIJA);
            item.setChecked(true);

            //Save the State Before Restarting
            saveMainActivityState();

            Intent intent = getIntent();
            finish();
            startActivity(intent);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
            }
            Toast.makeText(this, "Default", Toast.LENGTH_SHORT).show();
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_Search_mainMenu);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);


        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (null != searchManager) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // do your search on change or save the last string in search
                mainActivity_RecyclerAdapter.getFilter().filter(s);
                return true;
            }
        });

        return true;
    }

    private void sortCurrentListByRecentToOld() {
        sortManner = appConstants.MainActivity_ListSort_Mode_RecentToOld;
        if (NewsItemsList_Current != null) {
            Collections.sort(NewsItemsList_Current);
            mainActivity_RecyclerAdapter.notifyDataSetChanged();
        }
    }

    private void sortCurrentListByOldToRecent() {
        sortManner = appConstants.MainActivity_ListSort_Mode_OldToRecent;
        if (NewsItemsList_Current != null) {
            Collections.sort(NewsItemsList_Current, new Comparator<NewsItem>() {
                @Override
                public int compare(NewsItem lhs, NewsItem rhs) {

                    return rhs.compareTo(lhs);
                }
            });
            mainActivity_RecyclerAdapter.notifyDataSetChanged();
        }

    }

    private int UtilMethod_findRespectiveIndex_FromOriginalList (int currentIndex)
    {
        String itemTitle = mainActivity_RecyclerAdapter.getCurrentListBeingFiltered().get(currentIndex).title;
        for (int i = 0; i < CurrentNewsSource.getNewsItemArrayList().size() ; i++) {

            if (itemTitle.equals(CurrentNewsSource.getNewsItemArrayList().get(i).title))
            {
                return i;
            }
        }

        return 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_showRead:
                showAllReadOnly();
                return true;
            case R.id.action_markAllRead:
                markAllRead();
                return true;
            case R.id.action_deleteAllRead:
                deleteAllRead();
                return true;
            case R.id.action_sortByMostRecent:
                sortCurrentListByRecentToOld();
                return true;
            case R.id.action_sortByOldtoRecent:
                sortCurrentListByOldToRecent();
                return true;
            case R.id.subAction_GridLayout:
                globalClass.currentList_Style = R.layout.news_list_item_rectangular;
                setRecyclerView_ViewStyle(R.id.subAction_GridLayout);
                return true;
            case R.id.subAction_CardListLayout:
                globalClass.currentList_Style = R.layout.news_list_item_ascard;
                setRecyclerView_ViewStyle(R.id.subAction_CardListLayout);
                return true;
            case R.id.subAction_ListLayout:
                globalClass.currentList_Style = R.layout.news_list_item_simple;
                setRecyclerView_ViewStyle(R.id.subAction_ListLayout);
                return true;
            case R.id.subAction_CardMagazineLayout:
                globalClass.currentList_Style = R.layout.news_list_item_cardmagazine;
                setRecyclerView_ViewStyle(R.id.subAction_CardMagazineLayout);
                return true;
            case R.id.subAction_TileLayout:
                globalClass.currentList_Style = R.layout.news_list_item_rectangular;
                setRecyclerView_ViewStyle(R.id.subAction_TileLayout);
                return true;
//            case R.id.action_sortByOldtoRecent:
//                sortCurrentListByOldToRecent();
//                return true;
//            case R.id.action_sortByOldtoRecent:
//                sortCurrentListByOldToRecent();
//                return true;
//            case R.id.action_sortByOldtoRecent:
//                sortCurrentListByOldToRecent();
//                return true;
//            case R.id.action_sortByOldtoRecent:
//                sortCurrentListByOldToRecent();
//                return true;
        }


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {


        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onStop() {
        super.onStop();
        saveMainActivityState();
        ((globalClass) getApplication()).maintainPreviousState();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_setChannels) {
            Intent intent = new Intent(MainActivity.this, sourceSelectionAcitivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_Refresh) {

            Toast.makeText(MainActivity.this, "Sync started", Toast.LENGTH_SHORT).show();
            Toast.makeText(MainActivity.this, "Sync takes a while", Toast.LENGTH_SHORT).show();
            ((globalClass) getApplication()).makeCallToRetriveOnlineFeedForMe();

        } else if (id == R.id.nav_displaySettings)
        {

        } else if (id == R.id.nav_coreSettings) {
            Intent settingIntent = new Intent(MainActivity.this, CoreSettingsActivity.class);
            startActivity(settingIntent);

//        } else if (id == R.id.nav_Downloads) {
//            Intent theIntent = new Intent(MainActivity.this, downloadsActivity.class);
//            startActivity(theIntent);
//        }
        }else if (id == R.id.nav_About) {
            Intent theIntent = new Intent(MainActivity.this, AboutActivity.class);
            theIntent.putExtra(appConstants.IntentParam_AboutActivity_recognizesActivity,MainActivity.class.getName());
            startActivity(theIntent);
        } else {

            NavItemSelectionHandler(item);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void set_AllUnreads_ByDefault_WhenActivity_Created () {
        ifFavOrUnreadSelected = true;
        //Handling NewsSources Selection in the following Code
        ((TextView)findViewById(R.id.noChannelSelectedTextView)).setVisibility(View.GONE);
        NewsSource respectiveNewsSource = ((globalClass)getApplication()).getNewsSource_AllUnreads();
        CurrentNewsSource = respectiveNewsSource;
        getSupportActionBar().setTitle(CurrentNewsSource.sourcename + "(" + String.valueOf(CurrentNewsSource.getNewsItemArrayList().size()) + ")");
        if (respectiveNewsSource != null)
        {
            if (respectiveNewsSource.loadingState)
            {
                whoEnablesLoader = respectiveNewsSource.sourcename;
                //  progressBar.setVisibility(View.VISIBLE);
            }
            else
            {
                //  progressBar.setVisibility(View.GONE);
            }
            currentlySelectedNewsSourceName = respectiveNewsSource.sourcename;
            NewsItemsList_Current = respectiveNewsSource.getNewsItemArrayList();
            if (sortManner.equals(appConstants.MainActivity_ListSort_Mode_RecentToOld))
            {
                sortCurrentListByRecentToOld();
            }
            else
            {
                sortCurrentListByOldToRecent();
            }
            mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
            mainActivity_RecyclerAdapter.notifyDataSetChanged(); //notify that dataset has changed
            mRecylerView_NewsFeedList.smoothScrollToPosition(0); //Reset the ScrollBars position every time user selects new Source

        }
        else
        {
            ((TextView)findViewById(R.id.noChannelSelectedTextView)).setVisibility(View.VISIBLE);
        }
        if (respectiveNewsSource.getNewsItemArrayList().size()> 0)
        {
            //mHandler.removeCallbacks(mRunnable_For_mHandler);
        }
    }

    private void NavItemSelectionHandler(MenuItem item) {
        //Whenever a Source is selected get its data-ArrayList from globalClass and notify the ListView
        ((TextView)findViewById(R.id.noChannelSelectedTextView)).setVisibility(View.GONE);
        String tempTitle = item.getTitle().toString();
        ifFavOrUnreadSelected = false;
        if (tempTitle.equals(navMenu_Catergory_Favourites) || tempTitle.equals(navMenu_Catergory_AllUnreads)) {
            ifFavOrUnreadSelected = true;
            if (tempTitle.equals(navMenu_Catergory_AllUnreads)) {
                //Handling NewsSources Selection in the following Code
                NewsSource respectiveNewsSource = getUnreads_NewsSource();
                CurrentNewsSource = respectiveNewsSource;
                getSupportActionBar().setTitle(CurrentNewsSource.sourcename + "(" + String.valueOf(CurrentNewsSource.getNewsItemArrayList().size()) + ")");
                if (respectiveNewsSource != null) {
                    if (respectiveNewsSource.loadingState) {
                        whoEnablesLoader = tempTitle;
                        //  progressBar.setVisibility(View.VISIBLE);
                    } else {
                        //  progressBar.setVisibility(View.GONE);
                    }
                    currentlySelectedNewsSourceName = tempTitle;
                    NewsItemsList_Current = respectiveNewsSource.getNewsItemArrayList();
                    if (sortManner.equals(appConstants.MainActivity_ListSort_Mode_RecentToOld)) {
                        sortCurrentListByRecentToOld();
                    } else {
                        sortCurrentListByOldToRecent();
                    }
                    mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
                    mainActivity_RecyclerAdapter.notifyDataSetChanged(); //notify that dataset has changed
                    mRecylerView_NewsFeedList.smoothScrollToPosition(0); //Reset the ScrollBars position every time user selects new Source
                } else {

                    NewsItemsList_Current = null;
                    mainActivity_RecyclerAdapter.notifyDataSetChanged(); //notify that dataset has changed
                    mRecylerView_NewsFeedList.smoothScrollToPosition(0); //Reset the ScrollBars position every time user selects new Source

                }

            } else {
                //Handling NewsSources Selection in the following Code
                NewsSource respectiveNewsSource = getMyFavourites_NewsSource();
                CurrentNewsSource = respectiveNewsSource;
                getSupportActionBar().setTitle(CurrentNewsSource.sourcename + "(" + String.valueOf(CurrentNewsSource.getNewsItemArrayList().size()) + ")");
                if (respectiveNewsSource != null) {
                    if (respectiveNewsSource.loadingState) {
                        whoEnablesLoader = tempTitle;
                        //  progressBar.setVisibility(View.VISIBLE);
                    } else {
                        //  progressBar.setVisibility(View.GONE);
                    }
                    currentlySelectedNewsSourceName = tempTitle;
                    NewsItemsList_Current = respectiveNewsSource.getNewsItemArrayList();
                    if (sortManner.equals(appConstants.MainActivity_ListSort_Mode_RecentToOld)) {
                        sortCurrentListByRecentToOld();
                    } else {
                        sortCurrentListByOldToRecent();
                    }
                    mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
                    mainActivity_RecyclerAdapter.notifyDataSetChanged(); //notify that dataset has changed
                    mRecylerView_NewsFeedList.smoothScrollToPosition(0); //Reset the ScrollBars position every time user selects new Source
                } else {

                    NewsItemsList_Current = null;
                    mainActivity_RecyclerAdapter.notifyDataSetChanged(); //notify that dataset has changed
                    mRecylerView_NewsFeedList.smoothScrollToPosition(0); //Reset the ScrollBars position every time user selects new Source

                }

            }
            return;
        }
        //Set it checked and Set Title of Toolbar
        item.setChecked(true);
        getSupportActionBar().setTitle(tempTitle + "(" + ((TextView) item.getActionView().findViewById(R.id.actionViewText)).getText() + ")");
        //uncheck other navMenu items
        for (int i = 0; i < navMenuRef.size(); i++) {
            if (navMenuRef.getItem(i) != item) {
                navMenuRef.getItem(i).setChecked(false);
            }
        }

        //Handling NewsSources Selection in the following Code
        NewsSource respectiveNewsSource = ((globalClass) getApplication()).getNewsSourceByName(tempTitle);
        CurrentNewsSource = respectiveNewsSource;
        if (respectiveNewsSource != null) {
            if (respectiveNewsSource.loadingState) {
                whoEnablesLoader = tempTitle;
                //  progressBar.setVisibility(View.VISIBLE);
            } else {
                //  progressBar.setVisibility(View.GONE);
            }
            currentlySelectedNewsSourceName = tempTitle;
            NewsItemsList_Current = respectiveNewsSource.getNewsItemArrayList();
            if (sortManner.equals(appConstants.MainActivity_ListSort_Mode_RecentToOld)) {
                sortCurrentListByRecentToOld();
            } else {
                sortCurrentListByOldToRecent();
            }
            mainActivity_RecyclerAdapter.setListForRecyclerViewAdapter(NewsItemsList_Current);
            mainActivity_RecyclerAdapter.notifyDataSetChanged(); //notify that dataset has changed
            mRecylerView_NewsFeedList.smoothScrollToPosition(0); //Reset the ScrollBars position every time user selects new Source
        } else {

            NewsItemsList_Current = null;
            mainActivity_RecyclerAdapter.notifyDataSetChanged(); //notify that dataset has changed
            mRecylerView_NewsFeedList.smoothScrollToPosition(0); //Reset the ScrollBars position every time user selects new Source

        }

    }

    public void saveMainActivityState() {

        if (CurrentNewsSource != null) {
            if (ifFavOrUnreadSelected) {
                globalClass.MainActivityStatePreserveBundle_SourceName = currentlySelectedNewsSourceName;
            } else {
                globalClass.MainActivityStatePreserveBundle_SourceName = CurrentNewsSource.sourcename;
            }
        }

        if (NewsItemsList_Current != null) {
            ((globalClass) getApplication()).setNewsArrayList_WhichIs_Maintained(NewsItemsList_Current);
        }

        if (CurrentNewsSource == null && !ifFavOrUnreadSelected) {
            globalClass.MainActivityStatePreserveBundle_SourceName = null;
        }


    }


    //-------------------------------------RecyclerViewAdapter Class Starts Here-----------------------
    private class recylerAdapter extends RecyclerView.Adapter<recylerAdapter.customViewHolder> implements Filterable {
        ArrayList<NewsItem> mRecylerAdapterArrayList;
        ArrayList<NewsItem> mRecylerAdapterArrayList2;
        private final String toBeReplaced = "+0000";
        private final String toPutForReplacement = "";
        private NewsListFilter_RecyclerOne mFilter;
        private int pos_ContextMenu_CurrentlyFor = 0;


        public void setListForRecyclerViewAdapter(ArrayList<NewsItem> gottenListArray) {
            mRecylerAdapterArrayList = gottenListArray;
            mRecylerAdapterArrayList2 = gottenListArray;
        }

        public NewsItem getItem_customMethod(int poisiton) {
            return mRecylerAdapterArrayList.get(poisiton);
        }

        public int getPost_ContextMenu_CurentFor() {
            return pos_ContextMenu_CurrentlyFor;
        }

        @Override
        public customViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemLayout = getLayoutInflater().from(parent.getContext()).inflate(globalClass.currentList_Style, parent, false);
            return new customViewHolder(itemLayout);
        }

        @Override
        public int getItemCount() {

            if (mRecylerAdapterArrayList != null) {
                return mRecylerAdapterArrayList.size();
            }

            return 0;
        }

        @Override
        public void onBindViewHolder(customViewHolder holder, int position) {

            holder.mHeadlineText.setText(mRecylerAdapterArrayList.get(position).title);
            if (mRecylerAdapterArrayList.get(position).description!=null)
            {
                holder.mDescText.setText(mRecylerAdapterArrayList.get(position).description.trim());
            }
            else
            {
                holder.mDescText.setText("null");
            }


            holder.setLongClickListener(new appConstants.LongClickListener() {
                @Override
                public void onItemSelectedFromContextMenu(int position) {
                    pos_ContextMenu_CurrentlyFor = position;
                }
            });

            if (mRecylerAdapterArrayList.get(position).isRead == 0) {
                TypedValue mval = new TypedValue();
                MainActivity.this.getTheme().resolveAttribute(android.R.attr.textAppearanceLarge, mval, true);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.mHeadlineText.setTextAppearance(mval.resourceId);
                } else {
                    holder.mHeadlineText.setTextAppearance(MainActivity.this, mval.resourceId);
                }
            } else if (mRecylerAdapterArrayList.get(position).isRead == 1) {

                final int version = Build.VERSION.SDK_INT;
                int color;
                if (version >= 23) {
                    color = ContextCompat.getColor(MainActivity.this, R.color.ReadItemColor);
                } else {
                    color = getResources().getColor(R.color.ReadItemColor);
                }
                holder.mHeadlineText.setTextColor(color);
            }

            if (mRecylerAdapterArrayList.get(position).pubTime != null) {
                String tempTimeStringPresentedOne = mRecylerAdapterArrayList.get(position).pubTime;
                tempTimeStringPresentedOne = tempTimeStringPresentedOne.replace(toBeReplaced, toPutForReplacement);
                holder.mpubTimeText.setText(tempTimeStringPresentedOne);
            }
            if (mRecylerAdapterArrayList.get(position).imageLink != null) {
                holder.mImageView.setImageUrl(mRecylerAdapterArrayList.get(position).imageLink, ((globalClass) getApplication()).getImageLoader());
                holder.mImageView.setVisibility(View.VISIBLE);
            } else {
                holder.mImageView.setVisibility(View.GONE);

            }

        }

        @Override
        public Filter getFilter() {
            if (mFilter == null) {
                mFilter = new NewsListFilter_RecyclerOne();
            }
            return mFilter;
        }

        public ArrayList<NewsItem> getCurrentListBeingFiltered()
        {
            return mRecylerAdapterArrayList;
        }

        //--------------------ViewHolder Class Definition---------------------------
        public class customViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnCreateContextMenuListener {
            public TextView mHeadlineText;
            public TextView mDescText;
            public TextView mpubTimeText;
            public NetworkImageView mImageView;
            private appConstants.LongClickListener longClickListener;
            private int pos;

            public void setLongClickListener(appConstants.LongClickListener lc) {
                this.longClickListener = lc;
            }

            public customViewHolder(View itemView) {
                super(itemView);

                mHeadlineText = (TextView) itemView.findViewById(R.id.newsListItemText);
                mDescText = (TextView) itemView.findViewById(R.id.newsListItemDescriptionText);
                mpubTimeText = (TextView) itemView.findViewById(R.id.newsListItemTimeText);
                mImageView = (NetworkImageView) itemView.findViewById(R.id.newsListItemImage);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (ifSearchFilterActivated)
                        {
                            //From the index of the Filtered the list get the Index of Original List
                            int respectiveIndex = UtilMethod_findRespectiveIndex_FromOriginalList(getAdapterPosition());

                            Intent mIntent = new Intent(MainActivity.this, NewsDetailActivity.class);
                            //Mark Current Item Read in the Current List
                            CurrentNewsSource.getNewsItemArrayList().get(respectiveIndex).isRead = 1;
                            //Store the Scroll Position for the Recycler View
                            globalClass.MainActivityStatePreserveBundle_ScrollPosition = ((LinearLayoutManager) mRecylerView_NewsFeedList.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                            //Pass the Index of the Selected Item
                            mIntent.putExtra(IntentParam_ListIndex_MaintoDetailActivity, respectiveIndex);
                            //Give globalClass the CurrentNewsSource NextActivity Would Access it.
                            ((globalClass)getApplication()).setMainActivity_NewsDetailActivity_SharedNewsSource_SelectedNow(CurrentNewsSource);
                            //Start the NewsDetailActivity to show the Selected News
                            MainActivity.this.startActivity(mIntent);
                        }
                        else
                        {
                            Intent mIntent = new Intent(MainActivity.this, NewsDetailActivity.class);
                            //Mark Current Item Read in the Current List
                            NewsItemsList_Current.get(getAdapterPosition()).isRead = 1;
                            //Store the Scroll Position for the Recycler View
                            globalClass.MainActivityStatePreserveBundle_ScrollPosition = ((LinearLayoutManager) mRecylerView_NewsFeedList.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                            //Pass the Index of the Selected Item
                            mIntent.putExtra(IntentParam_ListIndex_MaintoDetailActivity, getAdapterPosition());
                            //Give globalClass the CurrentNewsSource NextActivity Would Access it.
                            ((globalClass)getApplication()).setMainActivity_NewsDetailActivity_SharedNewsSource_SelectedNow(CurrentNewsSource);
                            //Bundle b = new Bundle();
                            //b.putSerializable(appConstants.IntentParam_SourceItem_PassesMainActivity_to_NewsDetailActivity, CurrentNewsSource);
                            //mIntent.putExtra(appConstants.IntentParam_BundleKey_MainToDetialActivity, b);
                            //Start the NewsDetailActivity to show the Selected News
                            MainActivity.this.startActivity(mIntent);
                        }

                    }
                });

                itemView.setOnLongClickListener(this);
                itemView.setOnCreateContextMenuListener(this);
            }


            @Override
            public boolean onLongClick(View v) {
               // Toast.makeText(MainActivity.this, "onLongCLick Called", Toast.LENGTH_SHORT).show();
                pos = getLayoutPosition();
                this.longClickListener.onItemSelectedFromContextMenu(getLayoutPosition());
                return false;
            }

            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

                //Toast.makeText(MainActivity.this,"onCreateContextMenu Called",Toast.LENGTH_SHORT).show();
                NewsItem gottenItem = mainActivity_RecyclerAdapter.getItem_customMethod(pos);
                // if your column name is "name"
                menu.setHeaderTitle(gottenItem.title);
                MenuInflater inflater = getMenuInflater();
                getMenuInflater().inflate(R.menu.main_listview_item_contextmenu, menu);
            }


        }

        //-----------------------------------Filter Class Starts Here-----------------------------
        private class NewsListFilter_RecyclerOne extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults mResults = new FilterResults();

                //Setting following boolean to true
                MainActivity.this.ifSearchFilterActivated = true;
                if (constraint == null || constraint.length() == 0) {
                    synchronized (this) {
                        MainActivity.this.ifSearchFilterActivated = false;
                        mResults.values = mRecylerAdapterArrayList2;
                        mResults.count = mRecylerAdapterArrayList2.size();
                    }
                } else {
                    ArrayList<NewsItem> listContainingAll = new ArrayList<>();
                    ArrayList<NewsItem> constrainedList = new ArrayList<>();

                    synchronized (this) {
                        listContainingAll.addAll(mRecylerAdapterArrayList2);
                    }

                    for (int i = 0; i < listContainingAll.size(); i++) {
                        NewsItem m = listContainingAll.get(i);
                        if (m.title.toLowerCase().contains(constraint.toString().toLowerCase())) {
                            constrainedList.add(m);
                        }
                    }

                    mResults.values = constrainedList;
                    mResults.count = constrainedList.size();
                }

                return mResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mRecylerAdapterArrayList = (ArrayList<NewsItem>) results.values;
                notifyDataSetChanged();
            }
        }
    }

}
