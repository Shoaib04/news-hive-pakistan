package com.enigmaproapps.newshivepakistan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import static com.enigmaproapps.newshivepakistan.appConstants.NAME_APPTHEME_NAIJA;

public class ImageViewerActivity extends AppCompatActivity {

    String urlString;
    URL theImageUrl;
    Bitmap bitmap = null;
    ImageView mImageView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCorrectTheme();
        setContentView(R.layout.activity_image_viewer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mToolbarImageViewerActivity);
        toolbar.setBackgroundColor(Color.argb(30,255,255,255));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        urlString = getIntent().getStringExtra(appConstants.IntentParam_NewsDetailActivity_To_ImageViewerActivity);
        if (urlString == null) {
            Log.d("ImageViewerActivity", "urlString is NULL");
            return;
        }

        try {
            theImageUrl = new URL(urlString);
            mImageView = (ImageView) findViewById(R.id.imageViewerActivityImageView);
            ImageAsyncTask mTask = new ImageAsyncTask(mImageView, theImageUrl);
            mTask.execute();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }


    private void saveLoadedImage ()
    {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root+"/"+appConstants.IMAGEVIEWER_PHOTOSAVING_DIRECTORYNAME);
        String path = myDir.toString();
        if (!myDir.exists())
        {
            myDir.mkdirs();
        }

        String imageName = new Date().toString() +".jpg";
        myDir = new File(myDir,imageName);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(myDir);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            Toast.makeText(ImageViewerActivity.this,"Image saved to:"+path,Toast.LENGTH_SHORT).show();
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void setCorrectTheme() {
        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
        }
        String name = ((globalClass) getApplication()).getApp_CurrentThemeName();
        if (name.equals(NAME_APPTHEME_NAIJA)) {
            setTheme(R.style.AppThemeNaija_NoActionBar);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
            }
        }

        if (name.equals("AppThemeLight")) {
            setTheme(R.style.AppThemeLight);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark_Light));
            }
        }

        if (name.equals("AppThemeDark")) {
            setTheme(R.style.AppThemeDark);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.imageviewer_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id==android.R.id.home)
        {
            //finish();
            //NavUtils.navigateUpFromSameTask(this);
           // return true;
        }
        else if (id==R.id.downloadFeatureItem)
        {
            if (bitmap!=null)
            {
                saveLoadedImage();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class ImageAsyncTask extends AsyncTask<Void, Void, Bitmap> {
        ImageView theImage;
        URL theURL;

        public ImageAsyncTask(ImageView imgView, URL givenURL) {
            theImage = imgView;
            theURL = givenURL;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.imageViewerActivityTextView).setVisibility(View.GONE);
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                theImageUrl = new URL(urlString);

                URLConnection urlConnection = null;
                try {
                    urlConnection = theImageUrl.openConnection();
                    InputStream inputStream = urlConnection.getInputStream();
                    bitmap = BitmapFactory.decodeStream(inputStream);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            theImage.setImageBitmap(bitmap);
        }
    }


}

