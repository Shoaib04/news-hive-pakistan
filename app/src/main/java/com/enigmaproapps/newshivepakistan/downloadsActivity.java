package com.enigmaproapps.newshivepakistan;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import static com.enigmaproapps.newshivepakistan.appConstants.NAME_APPTHEME_NAIJA;

public class downloadsActivity extends AppCompatActivity {

    private ListView mListView;
    private File files[];
    private ArrayList<String> filesNames;
    private ArrayAdapter<String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCorrectTheme();
        setContentView(R.layout.activity_downloads);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toobar_downloadsActivity);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mListView = (ListView) findViewById(R.id.ListView_showDownloadActivity);

        setUpFilesList();
    }

    private void setUpFilesList() {
        String root = Environment.getExternalStorageDirectory().toString();
        String ourDirName = appConstants.IMAGEVIEWER_PHOTOSAVING_DIRECTORYNAME;
        final String completePath = root + "/" + ourDirName;

        File mDir = new File(completePath);

        if (mDir.exists())
        {
            files = mDir.listFiles();
            filesNames = new ArrayList<>();
            for (int i = 0; i < files.length; i++) {
                filesNames.add(files[i].toString());
            }
            mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, filesNames);
            mListView.setAdapter(mAdapter);

            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    String mimetype = "image/*";
                    String murl = files[position].toString();
                    String Url = "file://" + murl;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(Url), mimetype);
                    startActivity(intent);

                }
            });
        }
        else
        {
            TextView mTextView = (TextView) findViewById(R.id.TextView_downloadsActivity_NoContent);
            mTextView.setVisibility(View.VISIBLE);
        }

    }


    private void setCorrectTheme() {
        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
        }
        String name = ((globalClass) getApplication()).getApp_CurrentThemeName();
        if (name.equals(NAME_APPTHEME_NAIJA)) {
            setTheme(R.style.AppThemeNaija_NoActionBar);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
            }
        }

        if (name.equals("AppThemeLight")) {
            setTheme(R.style.AppThemeLight);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark_Light));
            }
        }

        if (name.equals("AppThemeDark")) {
            setTheme(R.style.AppThemeDark);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
            }
        }
    }

}
