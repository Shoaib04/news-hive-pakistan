package com.enigmaproapps.newshivepakistan;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;

import static com.enigmaproapps.newshivepakistan.appConstants.NAME_APPTHEME_NAIJA;

public class DetailActivityWithWebView extends AppCompatActivity {

    private WebView mWebView;
    private String linkToOpen;
    private final String LabelString = "News link copied";
    private String NewsTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCorrectTheme();
        setContentView(R.layout.detail_activity_with_web_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mActivityWithWebViewToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Get the title for News
        NewsTitle= getIntent().getStringExtra(appConstants.IntentParam_NewsDetailActivity_To_DetailActivityWithWebView1);
        //Get the url to open that has been sent by the previous Activity
        linkToOpen = getIntent().getStringExtra(appConstants.IntentParam_NewsDetailActivity_To_DetailActivityWithWebView);
        //Finding reference to the WebView from the layout
        mWebView = (WebView) findViewById(R.id.mWebViewToOpenLinks);
        //Set the toolBar title to NewsItem Title
        getSupportActionBar().setTitle(NewsTitle);
        //Now lets open the Url
        mWebView.loadUrl(linkToOpen);

    }

    @Override
    public boolean onNavigateUp() {

        return super.onNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detailactivity_with_webview,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_Refresh)
        {
            mWebView.reload();
        }
        else if (id == R.id.action_Share)
        {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = linkToOpen;
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }
        else if (id == R.id.action_copyLink)
        {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(LabelString, linkToOpen);
            clipboard.setPrimaryClip(clip);
        }
        else if(id == R.id.action_LaunchBrowser)
        {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(linkToOpen));
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    private void setCorrectTheme() {
        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
        }
        String name = ((globalClass) getApplication()).getApp_CurrentThemeName();
        if (name.equals(NAME_APPTHEME_NAIJA)) {
            setTheme(R.style.AppThemeNaija_NoActionBar);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
            }
        }

        if (name.equals("AppThemeLight")) {
            setTheme(R.style.AppThemeLight);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark_Light));
            }
        }

        if (name.equals("AppThemeDark")) {
            setTheme(R.style.AppThemeDark);
            // finally change the color
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary_Dark));
            }
        }
    }
}
